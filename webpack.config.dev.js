const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const Dotenv = require("dotenv-webpack");

module.exports = (env) => ({
  //entry: "./src/index.tsx",
  entry: {
    index: [path.resolve(__dirname, "src", "index.tsx")],
    index1: [path.resolve(__dirname, "src", "index1.tsx")],
    index_gray: [path.resolve(__dirname, "src", "index_gray.tsx")],
    react: [path.resolve(__dirname, "src", "react.tsx")],
  },
  //output: {
  //     path: path.join(__dirname, "build"), filename: "index.bundle.js"
  //},
  output: {
    filename: "js/[name].bundle.js",
    path: path.resolve(__dirname, "build"),
  },
  mode: "development",
  /* performance: {
    hints: false,
    maxEntrypointSize: 512000,
    maxAssetSize: 512000
  }, */
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
  },
  devServer: {
    static: path.join(__dirname, "src"),
    open: true,
    port: 8000,
    /* static: {
      directory: path.resolve(__dirname, "./dist"),
      publicPath: "/dist",
    }, */
    static: {
      directory: path.resolve(__dirname, "./public"),
      publicPath: "/public",
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: ["ts-loader"],
      },
      {
        test: /\.(css|scss)$/,
        //use: ["style-loader", "css-loader", "css-modules-typescript-loader"],
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(jpg|jpeg|png|gif|mp3|svg)$/,
        use: ["file-loader"],
      },
    ],
  },
  plugins: [
    //new HtmlWebpackPlugin({
    //    template: path.join(__dirname, "src", "index.html"),
    //}),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "src", "index.html"),
      filename: "index.html",
      chunks: ["index"],
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "src", "index1.html"),
      filename: "index1.html",
      chunks: ["index1"],
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "src", "index_gray.html"),
      filename: "index_gray.html",
      chunks: ["index_gray"],
    }),
    /*  new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "src", "react.html"),
      filename: "react.html",
      chunks: ["react"],
    }), */
    new Dotenv({
      path: `./environments/.env${env.file ? `.${env.file}` : ""}`,
    }),
  ],
  devtool: "inline-source-map",
});
