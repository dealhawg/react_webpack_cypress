import React from "react";
import { Header } from "./components/gray/Header";
import { Footer } from "./components/gray/Footer";
import { Home } from "./components/gray/Home";
import NewPost from "./components/gray/newpost/NewPost";
import { PostPage } from "./components/gray/PostPage";
import { EditPost } from "./components/gray/EditPost";
import { About } from "./components/gray/About";
import { Missing } from "./components/gray/Missing";
import { Route, Routes } from "react-router-dom";
import { DataProvider } from "./context/DataContext";
import { Layout } from "./components/gray/Layout";

//import Register from "./components/auth/Register";
import Login from "./components//auth/Login";

import Unauthorized from "./components/auth/Unauthorized";

//import LinkPage from "./components/auth/LinkPage";
import RequireAuth from "./components/auth/RequireAuth";

const ROLES = {
  User: 2001,
  Editor: 1984,
  Admin: 5150,
};

function App() {
  return (
    <div className="App">
      <Header title="HR Acuity" />
      <DataProvider>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route element={<RequireAuth allowedRoles={[ROLES.User]} />}>
              <Route index element={<Home />} />
            </Route>
            <Route path="post">
              <Route index element={<NewPost />} />
              <Route path=":id" element={<PostPage />} />
            </Route>
            {/* <Route element={<RequireAuth allowedRoles={[ROLES.Editor]} />}> */}
            <Route path="edit">
              <Route path=":id" element={<EditPost />} />
            </Route>
            {/* </Route> */}
            {/* public routes */}
            {/* <Route path="login" element={<Login />} /> */}

            {/* <Route path="linkpage" element={<LinkPage />} /> */}
            <Route path="unauthorized" element={<Unauthorized />} />
            <Route path="about" element={<About />} />
            <Route path="*" element={<Missing />} />
          </Route>
          <Route path="index_gray.html" element={<Layout />}>
            <Route index element={<Login />} />
          </Route>
        </Routes>
      </DataProvider>
      <Footer />
    </div>
  );
}

export default App;
