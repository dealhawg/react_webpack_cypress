import { useState, useEffect } from "react";
import axios from "axios";
import React from "react";

export const useAxiosFetch = (dataUrl: string) => {
  const [data, setData] = useState([]);
  const [fetchError, setFetchError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    let isMounted = true;
    const source = axios.CancelToken.source();

    const fetchData = async (url: string) => {
      setIsLoading(true);
      try {
        const response = await axios.get(url, {
          cancelToken: source.token,
        });
        if (isMounted) {
          setData(response.data);
          setFetchError("");
        }
      } catch (err) {
        if (isMounted) {
          if (typeof err === "string") {
            err.toUpperCase(); // works, `e` narrowed to string
            setFetchError(err.toUpperCase());
          } else if (err instanceof Error) {
            err.message; // works, `e` narrowed to Error
            setFetchError(err.message);
          }
          // setFetchError(err.message);
          setData([]);
        }
      } finally {
        isMounted && setIsLoading(false);
      }
    };

    fetchData(dataUrl);

    const cleanUp = () => {
      isMounted = false;
      source.cancel();
    };

    return cleanUp;
  }, [dataUrl]);

  return { data, fetchError, isLoading };
};

//export default useAxiosFetch;
