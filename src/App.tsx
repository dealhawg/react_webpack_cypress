import React from "react";
import { HelloWorld } from "./components/app/HelloWorld";
import RenderForm from "./components/app/RenderForm";
import { ThemeProvider } from "@material-ui/core";
import { createTheme } from "@material-ui/core/styles";
import { purple, green } from "@material-ui/core/colors";

const theme = createTheme({
  palette: {
    primary: {
      main: purple[500],
    },
    secondary: {
      main: green[500],
    },
  },
});

export const App: React.FC<{}> = () => {
  return (
    <ThemeProvider theme={theme}>
      <>
        <HelloWorld />
        <RenderForm />

        <header>
          <p>NODE_ENV: {process.env.FOO}</p>
          <p>TEST_VAR: {process.env.baseURL}</p>
          {/* <p>WEBSITE: {process.env.REACT_APP_WEBSITE}</p>
          <p>FILE_NAME: {process.env.REACT_APP_FILE_NAME}</p> */}
        </header>
      </>
    </ThemeProvider>
  );
};

//export default App;
