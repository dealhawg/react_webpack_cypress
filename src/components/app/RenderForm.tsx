import React, { CSSProperties, useState } from "react";
import { Button } from "@material-ui/core";
import AcUnitOutlinedIcon from '@material-ui/icons/AcUnitOutlined';
import SendIcon from '@material-ui/icons/Send';
import { TextField } from "@material-ui/core";

const RenderForm: React.FC<{}> = () => {
  const [name, setName] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  const [showDetails, setShowDetails] = useState<boolean>(false);

  const handleNameChange = (event: any): void => {
    setName(event.currentTarget.value);
    setShowDetails(false);
  };

  const handleEmailChange = (event: any): void => {
    setEmail(event.currentTarget.value);
    setShowDetails(false);
  };

  const handleSubmit = (event: any) => {
    event?.preventDefault();
    if (name && email) {
      setShowDetails(true);
    }
  };

  const inputStyles: CSSProperties = {
    marginLeft: "0.5rem",
    marginTop: "1rem",
    marginBottom: "1rem",
    height: "24px",
    width: "300px",
    position: "relative",
    bottom: "0.1rem",
    fontSize: "18px",
  };

  const formStyles: CSSProperties = {
    backgroundColor: "lightblue",
    display: "block",
    margin: "1rem",
    paddingLeft: "1rem",
  };

  const containerStyles: CSSProperties = {
    margin: "1rem",
    textAlign: "center",
    fontSize: "24px",
  };

  const submitButtonStyle: CSSProperties = {
    fontSize: "20px",
    marginBottom: "1rem",
  };

  return (
    <div style={containerStyles}>
      <label>Input Form:</label>
      <form style={formStyles}>
        <div>
          <TextField 
            id="outlined-basic" 
            label="Name:" 
            variant="outlined"
            onChange={handleNameChange} />
        </div>
        <br></br>
        <div>
          <TextField 
            type="email"
            id="outlined-basic" 
            label="Email:" 
            variant="outlined"
            onChange={handleEmailChange}
            multiline />
        </div>
        {/* <input style={submitButtonStyle} onClick={handleSubmit} type="submit" /> */}
        <Button variant="contained" color="primary" onClick={handleSubmit} type="submit" endIcon={<SendIcon />}> Primary</Button>
        <AcUnitOutlinedIcon />

      </form>
      {showDetails ? (
        <div>
          <p>Name: {name}</p>
          <p>Email: {email}</p>
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

export default RenderForm;
