import { useParams, Link, useNavigate } from "react-router-dom";
import { useContext } from "react";
import api from "../../../api/posts";
import { DataContext } from "../../context/DataContext";
import React from "react";
interface Post {
  id: number;
  body: string;
  title: string;
}
export const PostPage = () => {
  const { posts, setPosts } = useContext(DataContext);
  const { id } = useParams<{ id?: string }>();
  const navigate = useNavigate();
  const post = posts.find((post: Post) => post.id.toString() === id);

  const handleDelete = async (id: number) => {
    try {
      await api.delete(`/posts/${id}`);
      const postsList = posts.filter((post: Post) => post.id !== id);
      setPosts(postsList);
      navigate("/");
    } catch (err) {
      if (typeof err === "string") {
        err.toUpperCase(); // works, `e` narrowed to string
        console.log(`Error: ${err.toUpperCase()}`);
      } else if (err instanceof Error) {
        err.message; // works, `e` narrowed to Error                ;
        console.log(`Error: ${err.message}`);
      }
    }
  };

  return (
    <main className="PostPage">
      <article className="post">
        {post && (
          <>
            <h2>{post.title}</h2>
            <p className="postDate">{post.datetime}</p>
            <p className="postBody">{post.body}</p>
            <Link to={`/edit/${post.id}`}>
              <button className="editButton">Edit Post</button>
            </Link>
            <button
              className="deleteButton"
              onClick={() => handleDelete(post.id)}
            >
              Delete Post
            </button>
          </>
        )}
        {!post && (
          <>
            <h2>Post Not Found</h2>
            <p>Well, that's disappointing.</p>
            <p>
              <Link to="/">Visit Our Homepage</Link>
            </p>
          </>
        )}
      </article>
    </main>
  );
};

//export default PostPage
