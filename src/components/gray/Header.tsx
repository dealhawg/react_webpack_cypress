import { FaLaptop, FaTabletAlt, FaMobileAlt } from "react-icons/fa";
import { useWindowSize } from "../../hooks/useWindowSize";
import React from "react";

interface Header {
  title: string;
}
export const Header: React.FC<Header> = ({ title }) => {
  const { width } = useWindowSize();

  return (
    <header className="Header">
      <h1>{title}</h1>
      {width! < 768 ? (
        <FaMobileAlt />
      ) : width! < 992 ? (
        <FaTabletAlt />
      ) : (
        <FaLaptop />
      )}
    </header>
  );
};

//export default Header
