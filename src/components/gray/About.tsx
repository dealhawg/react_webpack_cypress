export const About = () => {
  return (
    <main className="About">
      <h2>About</h2>
      {/*  <p style={{ marginTop: "1rem" }}>
        This blog app is a project in the Learn React tutorial series.
      </p> */}
      <div
        style={{
          position: "relative",
          height: 0,
          paddingTop: "25px",
          paddingBottom: "56.25%",
        }}
      >
        <iframe
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            width: "100%",
            height: "100%",
          }}
          src="https://react-13.netlify.app/react.html"
        ></iframe>
      </div>
    </main>
  );
};

//export default About
