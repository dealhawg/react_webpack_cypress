import { Link } from "react-router-dom";
import React from "react";
import { Grid, Paper, Container } from "@material-ui/core";

interface IPost {
  post: {
    id: number;
    body: string;
    title: string;
    datetime: string;
  };
}
export const Post: React.FC<IPost> = ({ post }) => {
  return (
    <>
      {/* <article className="post">
                <Link to={`/post/${post.id}`}>
                    <h2>{post.title}</h2>
                    <p className="postDate">{post.datetime}</p>
                </Link>
                <p className="postBody">{
                    (post.body).length <= 25
                        ? post.body
                        : `${(post.body).slice(0, 25)}...`
                }</p>
            </article> */}
      <Container>
        <Grid container>
          <Grid item xs={12} sm={6} md={4}>
            <Paper>
              <Link to={`/post/${post.id}`}>
                <h2>{post.title}</h2>
              </Link>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6} md={4}>
            <Paper>
              {" "}
              <p>{post.datetime}</p>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={6} md={4}>
            <Paper>
              <p>
                {post.body.length <= 25
                  ? post.body
                  : `${post.body.slice(0, 25)}...`}
              </p>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

//export default Post
