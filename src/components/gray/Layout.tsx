import { Nav } from "./Nav";
import { Outlet } from "react-router-dom";

export const Layout: React.FC<{}> = () => {
  return (
    <div className="App">
      <Nav />
      <Outlet />
    </div>
  );
};

//export default Layout
