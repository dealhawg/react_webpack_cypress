import { Post } from "./Post";
import React from "react";

interface IFeed {
  posts: {
    id: number;
    body: string;
    title: string;
    datetime: string;
  }[];
}

export const Feed: React.FC<IFeed> = ({ posts }) => {
  return (
    <>
      {posts.map((post) => (
        <Post key={post.id} post={post} />
      ))}
    </>
  );
};

//export default Feed
