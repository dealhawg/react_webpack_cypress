import { createContext, useState } from "react";

interface Context {
  auth: any;
  setAuth: any;
}
const AuthContext = createContext<Partial<Context>>({});

export const AuthProvider: React.FC<{}> = ({ children }) => {
  const [auth, setAuth] = useState<Context>();

  return (
    <AuthContext.Provider value={{ auth, setAuth }}>
      {children}
    </AuthContext.Provider>
  );
};

export default AuthContext;
