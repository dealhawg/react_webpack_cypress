import { createContext, useState, useEffect  } from 'react';
import {useAxiosFetch} from '../hooks/useAxiosFetch';
import React  from "react";

interface Post{    
    body: string
    title: string        
}
interface SiteContext {    
    searchResults: any, 
    fetchError : string, 
    isLoading: boolean, 
    search: string,
    setSearch: any,
    posts:any,
    setPosts: any
}
export const DataContext = createContext<Partial<SiteContext>>({});

export const DataProvider : React.FC<{}> = ({ children })  => {
    const [posts, setPosts] = useState([])
    const [search, setSearch] = useState('');
    const [searchResults, setSearchResults] = useState([]);
    const urlBase = process.env.baseURL + '/posts';

    //const { data, fetchError, isLoading } = useAxiosFetch('http://localhost:3500/posts');
    //const { data, fetchError, isLoading } = useAxiosFetch('https://my-json-server.typicode.com/pgarge/react/posts');
    const { data, fetchError, isLoading } = useAxiosFetch(urlBase);

    
    console.log(urlBase);
    useEffect(() => {
        setPosts(data);
    }, [data])

    useEffect(() => {
        const filteredResults = posts.filter((post : Post) =>
            ((post.body).toLowerCase()).includes(search.toLowerCase())
            || ((post.title).toLowerCase()).includes(search.toLowerCase()));

        setSearchResults(filteredResults.reverse());
    }, [posts, search])

    return (
        <DataContext.Provider value={{
            search, setSearch,
            searchResults, fetchError, isLoading,
            posts, setPosts
        }}>
            {children}
        </DataContext.Provider>
    )
}

//export default DataContext;