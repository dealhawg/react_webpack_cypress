import axios from 'axios';

export default axios.create({
    //baseURL: 'http://localhost:3500'
    //baseURL:'https://my-json-server.typicode.com/pgarge/react'
    baseURL: process.env.baseURL
});